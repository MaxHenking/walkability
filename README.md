# Planification d'itinéraires pour piétons-nnes

Melinda Femminis, Max Henking, Baptiste Poffet | *Géovisualisation dynamique et traitement de données (Géovis 2)* | UNIL

Utiliser la première version de l'application: [https://melindafemminis.github.io/walkability-mapbox/](https://melindafemminis.github.io/walkability-mapbox/)

## Objectif et public cible

Le but de ce projet est de créer une application permettant aux personnes à pied de planifier leur itinéraire dans la région Lausannoise. Les users pourront voir visualiser l'itinéraire, savoir sa distance et le temps qu'il faudra pour le parcourir. L'avantage d'une applications spécifique aux piétons par rapport à une calcul d'itinéraire standard est que les routes et chemins empruntés sont adaptés: pas d'autoroute, préférence pour les chemins piétons. 

La public cible sont les personnes ne connaissant pas la ville et souhaitant se déplacer à pied d'une manière agréable ainsi que celles qui la connaissent déjà mais voudraient un intinéraire potentiellement plus adapté. 

## Première version

Cette première version se trouve sur la branche **mapbox** du repository. 

### Fonctionnalités minimales

Le but de la première version et de créer une application qui fonctionne et qui soit utilisable mais qui ne contient pas encore toutes les fonctionnalités et détails qui seront implémentés dans la version finale.

Fonctionnalités minimales: 

- une application accessible via un navigateur web sur ordinateur (smartphones et petits écrans non pris en charge)
- un layout simple: un titre, une carte centrée sur Lausanne avec une barre d'options sur le côté
- trois boutons dans la barre d'options: un bouton **Start** pour placer un marqueur de départ en cliquant sur la carte, un marqueur **End** poru placer un marqueur d'arrivée sur la carte et un bouton **Routing** pour lancer la fonctione de calcul d'initéraire
- pas plus d'un marqueur de début et un marqueur de fin doivent pouvoir être placés sur la carte à la fois
- la fonction de calcul d'itinéraire doit être adaptée aux piétons

### Données et outils utilisés

- La librairie Javascript [Leaflet](https://leafletjs.com) pour la création de la carte
- [Maxbox Directions API](https://docs.mapbox.com/api/navigation/directions/) pour le calcul d'itinéraire 

> Mabbox nécessite la création d'un compte et l'ajout d'une clé personnelle pour avoir accès aux fonctionnalités

- [Open Street Map](https://www.openstreetmap.org/#map=8/46.825/8.224) pour le fond de carte

### Résultat 

![](img_readme/v1_1.png)

Cette première version implémente les différentes fonctionnalités minimales mentionnées plus haut ainsi que des fonctionnalités extras: 

- deux façon de placer les marqueurs de début et de fin de trajet: avec les boutons *Point de départ* et *Destination* de la colonne de droite qui permettent de cliquer sur la carte ou en recherchant une adresse exacte avec les basse de recherche au-dessus de la carte
- une fois un marqueur de début et un marqueur de fin défini, la fonction de routing se lance automatiquement
- l'user peut choisir le biai de l'algorithme de routing: *Bas* l'influence contre les routes définies comme *walkway* dans OSM, *Normal* est neutre et *Haut* préfère fortement les walkways
- l'user peut aussi définir la vitesse de marche/course
- ces informations lui sont retournées lorque le bouton *Afficher les statistiques* est cliqué. 

![](img_readme/v1_2.png)

Ce qui fait que cet algorithme est spécifique aux piétons est que le profil "*walking*" a été défini dans les paramètres Mapbox. Le biais de -1, 0 et +1 (correspiondant aux catérogies bas, normal et haut) permette de modifier ce profile tout en restant sur des routes accessibles aux piétons uniquement. 

## Deuxième version

La deuxième version se trouve sur la branch **pg** du repository. 

La première version est fonctionnelle mais pose des limitations, la principale étant l'impossibilité de modifier manuellement l'algorithme de Mapbox. 

### Fonctionnalités

Cette version se base sur la première mais implémente de nouvelle fonctionnalités: 

- implémentation d'une base de données PostGIS avec les données OSM
- utilisation de l'extension *pgRouting* afin de pouvoir choisir et modifier l'algorithme utilisé selon les attributs de chaque chemin utilisé par OSM. 

### Données et outils utilsés

- La librairie Javascript [Leaflet](https://leafletjs.com) pour la création de la carte
- [Open Street Map](https://www.openstreetmap.org/#map=8/46.825/8.224) pour le fond de carte
- [PostgreSQL v14](https://www.postgresql.org) avec les extensions [postgis](http://www.postgis.us) et [pgrouting](https://pgrouting.org)
- le fichier *roads_lausanne.geojson* qui a été récupéré à **XXX**
- [ogr2ogr](https://gdal.org/programs/ogr2ogr.html) a command line tool provided by GDAL to convert geospatial data format

### Installation pas à pas

#### Préparation des données topologiques

Pour MacOS depuis une instance du terminal, avec [homebrew](https://brew.sh):

1. `$ brew install postgresql@14` pour installer PostgreSQL v14
2. `$ brew install postgis` puis `$ brew install pgrouting` pour installer les extensions
3. `$ initdb ~/my-postgres-server -E utf8`, `$ pg_ctl -D ~/my-postgres-server -l logfile start` et `$ createdb my-db` pour initialiser une nouvelle base de données
4. `$ psql routedb -c "CREATE EXTENSION postgis;"` et `$ psql routedb -c "CREATE EXTENSION pgrouting;"` pour y ajouter les extensions qui permettent de gérer les données spatiales
5. Avec le fichier *roads_lausanne.geojson* dans le dossier de travail, `$ ogr2ogr -select 'bicycle,name,highway,lit,oneway,surface,surface' -lco GEOMETRY_NAME=the_geom -lco FID=id -f PostgreSQL PG:"dbname=routedb user=USERNAME" -nln edges roads_lausanne.geojson`. *L'username est celui de PostgreSQL.*
6. Le réseau doit contenir uniquement des géométrie ligne - et non des polygones. Utiliser `$ psql routedb -c "DELETE FROM edges WHERE ST_geometrytype(the_geom) = 'ST_Polygon';"`
7. Création de noeuds aux intersections des segmetnts: `$ psql routedb -c "psql routedb -c "SELECT pgr_nodeNetwork('edges', 0.00001);"`
8. Construire la topologie: `$ psql routedb -c "SELECT pgr_createTopology('edges_noded', 0.00001);"`
9. Mettre à jour la nouvelle table avec les attributs de l'ancienne: 

```
$ psql routedb -c "ALTER TABLE edges_noded
 ADD COLUMN name VARCHAR,
 ADD COLUMN type VARCHAR;
 ADD COLUMN type VARCHAR;

UPDATE edges_noded AS new
 SET name=old.name, 
   type=old.highway 
FROM edges as old
WHERE new.old_id=old.id;"
```
> Il est possible d'inpecter un ficheir geojson avec le site geojson.io et d'y voir les attributs. *Bicycle, name, highway, lit, oneway* et *surface* dans la commande ci-dessus correspondent à des attributs. 

#### Lancer l'application Flask

Télécharger la branche **pg** du repository. Créer localement un nouvel environnement python et y installer les librairies nécessaires avec la commande `$ pip install -r requirements.txt`. Lancer l'application avec `$ flask run` et l'ouvrir dans un navigateur.

#### Résultat 

L'interface graphique est la même que pour la version **mapbox**. Le calcul d'itinéraire se fait de la même manière pour l'instant. 

La différence est que l'application fonctionne maintenant dans un environnement Flask est le fichier de départ est *app.py*. C'est dans celui-ci que se fait le lien avec la base de données. Le fichier *.geojson* de la ville de Lausanne est consultable en ajoutant `/lausannegeojson` à l'URL. 

## Suite

Plusieurs améliorations sont possibles pour une prochaines version (disponible dans l'onglet *Issues* de ce repository): 

- Effectuer le lien entre le réseau de routes de la ville de Lausanne. *Pgouting* propose plusieurs algorithmes, dont celui de Djikstra qui semble approprié. 
- Modifer la façopn de calculer le coût afin de prendre mieux en compte les chemins piétonniers.
- Rendre l'application accessible via différentes plateformes, que cela soit sur écran d'ordinateur ou de smartphone


 



